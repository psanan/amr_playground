#!/usr/bin/env python
# Solve a 2D stokes benchmark problem, using a quadtree mesh

from reset_helper import reset_helper
reset_helper()

import sys
import scipy
from scipy.sparse.linalg import spsolve
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from timer import Timer
from refine_funcs import *
from stokes_funcs import *
from corner_funcs import *
from stokesproblem2d import StokesProblem2D
from solkz import SolKz
from solcx import SolCx

# Main Parameters ##############################################################

sol = SolKz()
#sol = SolCx()

max_level = 6

#refine_function = refine_function_uniform
refine_function = refine_function_test
#refine_function = refine_function_layers
#refine_function = refine_function_gmd_fig3

################################################################################

np.set_printoptions(threshold=np.inf)

# Plotting setup
figsize_one_by_three = (11,3)
figsize_square = (9,9)
#plt.set_cmap('RdBu')

# Create an object which manages our Stokes problem data.
# In this script, this is a bit overkill, but this allows
# us to solve and analyze sequences of problems in other scripts
pr = StokesProblem2D()

# Set the benchmark (parameters and exact solution)
pr.benchmark = sol

# Refine the mesh
pr.tree.refine(lambda cell: refine_function(cell,max_level))
# note that you could also use pr.tree.refine_uniform() after this

# Set up the problem. After this, you can't adjust the mesh or problem parameters
# anymore. Before this, you can't compute the solution.
pr.setup()
print(pr.elements,"elements and",pr.faces,"faces")

# Draw the grid
if max_level <= 5:
    with Timer('Plot tree'):
        fig,ax = plt.subplots(figsize=figsize_square)
        pr.tree.draw(ax,draw_numbers=max_level<=4)
        plt.show(block=False)

## Draw the corners (pretty!)
#if True:
#    with Timer('Plot corners'):
#        fig,ax = plt.subplots(figsize=figsize_square)
#        pr.tree.draw(ax)
#        draw_corners(ax,pr.corners)
#        pr.tree.draw_corners(ax)

# Compute the exact solution, evaluated at the cell and face centers
# Translate the pressure so that it's zero in the upper left corner
pr.populate_exact_solution()
p_exact_norm_inf = scipy.linalg.norm(pr.p_exact,ord=np.inf)
v_exact_norm_inf = scipy.linalg.norm(pr.v_exact,ord=np.inf)

# Plot the exact solution
with Timer("Plot exact solution"):
    fig,axes = plt.subplots(1,3,figsize=figsize_one_by_three)
    pr.tree.draw_element_vector(axes[0],pr.p_exact)
    axes[0].set_title('p (exact)')
    pr.tree.draw_face_vector_x(axes[1],pr.v_exact)
    axes[1].set_title('vx (exact)')
    pr.tree.draw_face_vector_y(axes[2],pr.v_exact)
    axes[2].set_title('vy (exact)')
    plt.tight_layout()

# Populate and plot the viscosity
if True:
    with Timer('Populate extra viscosity vector'):
        eta_element = np.empty(pr.elements)
        eta_element[:] = np.nan
        pr.tree.populate_element_vector(eta_element,pr.benchmark.eta,offset=-pr.faces)
    with Timer('Plot viscosity'):
        fig, ax = plt.subplots(figsize=figsize_square)
        pr.tree.draw_element_vector(ax,eta_element)
        pr.tree.draw(ax)
        ax.set_title('eta (elements)')

# Populate Stokes System
with Timer('Populate Stokes System'):
    pr.populate_stokes_system()

print('Kbnd=',pr.Kbnd,'Kcont=',pr.Kcont)
print("A has size",pr.A.get_shape()," and ",pr.A.nnz,"nonzero entries")

# Solve
with Timer('Solve'):
    pr.solve()

# Plot Solution
with Timer('Plot Solution'):
    fig,axes = plt.subplots(1,3,figsize=figsize_one_by_three)
    pr.tree.draw_element_vector(axes[0],pr.p)
    axes[0].set_title('Pressure')
    pr.tree.draw_face_vector_x(axes[1],pr.v)
    axes[1].set_title('X-velocity')
    pr.tree.draw_face_vector_y(axes[2],pr.v)
    #pr.tree.draw(axes[2])
    axes[2].set_title('Y-velocity ')
    plt.tight_layout()

# Plot Error
with Timer("Plot error at cell/face centers"):
    fig,axes = plt.subplots(1,3,figsize=figsize_one_by_three)
    pr.tree.draw_element_vector(axes[0],(pr.p_exact-pr.p)/p_exact_norm_inf)
    axes[0].set_title('pr.exact - pr.(normalized)')
    pr.tree.draw_face_vector_x(axes[1],(pr.v-pr.v_exact)/v_exact_norm_inf)
    axes[1].set_title('vx_exact - vx(normalized)')
    pr.tree.draw_face_vector_y(axes[2],(pr.v-pr.v_exact)/v_exact_norm_inf)
    axes[2].set_title('vy_exact - vy (normalized)')
    plt.tight_layout()

# Show all plots
plt.show(block=False)
