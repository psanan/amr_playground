import matplotlib.patches as patches

def populate_corners(cell,corners):
    # process root cell, then recurse

    # Note - we could also populate on non-leaves, to have element-->corner on all levels
    if cell.is_leaf():
        # Convert to index on a fine level (note 1 extra index for corners)
        if cell.level > Corner.max_level:
            raise Exception("increase Corner max_level")
        level_factor = 2**(Corner.max_level - cell.level)
        cidx_x = level_factor * cell.idx_x
        cidx_y = level_factor * cell.idx_y

        # Add down left corner
        idx = (cidx_x,cidx_y)
        if idx not in corners:
            corners[idx] = Corner(idx)
        corners[idx].cellB = cell
        cell.corner_downleft = corners[idx]

        # Add down right corner
        idx = (cidx_x+level_factor,cidx_y)
        if idx not in corners:
            corners[idx] = Corner(idx)
        corners[idx].cellA = cell
        cell.corner_downright = corners[idx]

        # Add up right corner
        idx = (cidx_x+level_factor,cidx_y+level_factor)
        if idx not in corners:
            corners[idx] = Corner(idx)
        corners[idx].cellC = cell
        cell.corner_upright = corners[idx]

        # Add up left corner
        idx = (cidx_x,cidx_y+level_factor)
        if idx not in corners:
            corners[idx] = Corner(idx)
        corners[idx].cellD = cell
        cell.corner_upleft = corners[idx]

        # Define hanging corners. Detect from the "small side" (redundantly) but pointers are to "big side"
        # Note: we only detect from one of the two cells on the "small side"
        if not cell.boundary_right and not cell.neighbor_right:
            if cell.pos in [0,2]:
                raise Exception("Assertion Failure")
            if cell.pos == 1:
                idx = (cidx_x + level_factor,cidx_y)
                if idx not in corners:
                    corners[idx] = Corner(idx)
                corners[idx].cellHright = cell.parent.neighbor_right
                corners[idx].hanging = True
                cell.parent.neighbor_right.hang_left = corners[idx]

        if not cell.boundary_left and not cell.neighbor_left:
            if cell.pos in [1,3]:
                raise Exception("Assertion Failure")
            if cell.pos == 0:
                idx = (cidx_x,cidx_y)
                if idx not in corners:
                    corners[idx] = Corner(idx)
                corners[idx].cellHleft = cell.parent.neighbor_left
                corners[idx].hanging = True
                cell.parent.neighbor_left.hang_right = corners[idx]

        if not cell.boundary_up and not cell.neighbor_up:
            if cell.pos in [2,3]:
                raise Exception("Assertion Failure")
            if cell.pos == 0:
                idx = (cidx_x + level_factor,cidx_y + level_factor)
                if idx not in corners:
                    corners[idx] = Corner(idx)
                corners[idx].cellHup = cell.parent.neighbor_up
                corners[idx].hanging = True
                cell.parent.neighbor_up.hang_down = corners[idx]

        if not cell.boundary_down and not cell.neighbor_down:
            if cell.pos in [0,1]:
                raise Exception("Assertion Failure")
            if cell.pos == 2:
                idx = (cidx_x + level_factor,cidx_y)
                if idx not in corners:
                    corners[idx] = Corner(idx)
                corners[idx].cellHdown = cell.parent.neighbor_down
                corners[idx].hanging = True
                cell.parent.neighbor_down.hang_up = corners[idx]

    else:
        for child in cell.children:
            populate_corners(child,corners)

def draw_corners(ax,corners):
    """ Draw lines from the corners to the centers of adjacent cells """
    denom = float(2**Corner.max_level)
    connectionstyle = 'arc3,rad=0.25'
    for corner in corners.values():
        x = corner.cidx_x/denom
        y = corner.cidx_y/denom
        if corner.cellA:
            ax.add_patch(patches.FancyArrowPatch((x,y),(corner.cellA.x_center,corner.cellA.y_center),connectionstyle=connectionstyle,color='r'))
        if corner.cellB:
            ax.add_patch(patches.FancyArrowPatch((x,y),(corner.cellB.x_center,corner.cellB.y_center),connectionstyle=connectionstyle,color='g'))
        if corner.cellC:
            ax.add_patch(patches.FancyArrowPatch((x,y),(corner.cellC.x_center,corner.cellC.y_center),connectionstyle=connectionstyle,color='b'))
        if corner.cellD:
            ax.add_patch(patches.FancyArrowPatch((x,y),(corner.cellD.x_center,corner.cellD.y_center),connectionstyle=connectionstyle,color='m'))
        if corner.cellHup:
            ax.add_patch(patches.FancyArrowPatch((x,y),(corner.cellHup.x_center,corner.cellHup.y_center),connectionstyle=connectionstyle,color='grey'))
        if corner.cellHdown:
            ax.add_patch(patches.FancyArrowPatch((x,y),(corner.cellHdown.x_center,corner.cellHdown.y_center),connectionstyle=connectionstyle,color='orange'))
        if corner.cellHleft:
            ax.add_patch(patches.FancyArrowPatch((x,y),(corner.cellHleft.x_center,corner.cellHleft.y_center),connectionstyle=connectionstyle,color='y'))
        if corner.cellHright:
            ax.add_patch(patches.FancyArrowPatch((x,y),(corner.cellHright.x_center,corner.cellHright.y_center),connectionstyle=connectionstyle,color='c'))

################################################################################
class Corner(object):
    max_level = 10 # 2^max_level shouldn't overflow
    def __init__(self,idx):
        self.cidx_x = idx[0]
        self.cidx_y = idx[1]
        self.cellA = None
        self.cellB = None
        self.cellC = None
        self.cellD = None
        self.cellHleft = None
        self.cellHright = None
        self.cellHup = None
        self.cellHdown = None
        self.hanging = False # False until proven otherwise
