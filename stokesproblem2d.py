import scipy.sparse
import scipy.sparse.linalg

from timer import Timer
from refine_funcs import *
from stokes_funcs import *
from corner_funcs import *
from quadtree import QuadTreeCell
from solkz import SolKz

class StokesProblem2D:
    """ Collect information to discretize, solve, and analyze a 2D,
        variable-viscosity Stokes problem.

        Workflow:
            - Create a new object
            - Change things, which can only be done pre-setup, in particular
                - refine the grid
                - set the benchmark
            - Set up (which can only be done once)
            - Perform additional operations, only available post-setup
                - populate the Stokes system
                - compute the approximate solution
                - compute the reference solution

        """

    def __init__(self):
        """ Create a new problem, with many fields undefined """
        self.name      = 'Unnamed Stokes Problem 2D'
        self.set_up    = False
        self.system_populated = False
        self.tree      = QuadTreeCell() # single, unrefined element
        self.elements  = None
        self.faces     = None
        self.corners   = None
        self.benchmark = SolKz()   # A default, usually override
        self.A         = None
        self.rhs       = None
        self.sol       = None # has scaled pressures
        self.p         = None # pressures w/o scaling
        self.v         = None
        self.p_exact   = None
        self.v_exact   = None

    def populate_exact_solution(self):
        """ Compute the exact solution, evaluated at the cell and face centers
            Translate the pressure so that it's zero in the upper left corner """

        if not self.set_up:
            raise Exception("Problem must be set up")
        if self.p_exact and self.v_exact:
            print("Warning: not re-computing exact solution")
            return
        self.p_exact = np.empty(self.elements); self.p_exact[:] = np.nan
        self.tree.populate_element_vector(self.p_exact,self.benchmark.exact_p,offset=-self.faces)
        pinned_pressure_dof = self.tree.get_upper_right_cell().dof_element - self.faces
        self.p_exact = self.p_exact - self.p_exact[pinned_pressure_dof] # subtract value of pinned pressure
        self.v_exact = np.empty(self.faces); self.v_exact[:] = np.nan
        self.tree.populate_face_vector_x(self.v_exact,self.benchmark.exact_vx)
        self.tree.populate_face_vector_y(self.v_exact,self.benchmark.exact_vy)

    def populate_stokes_system(self, variant = 'GMD13_A'):
        if not self.set_up:
            raise Exception("Problem must be set up")
        if self.system_populated:
            raise Exception("System already populated")
        eta_min = self.benchmark.eta_min()
        eta_max = self.benchmark.eta_max()
        eta_ref = np.sqrt(eta_min * eta_max)
        h_min = 2.0**(-self.tree.get_max_level())
        self.Kbnd  = eta_ref/(h_min**2)
        self.Kcont = eta_ref/h_min

        n = self.elements + self.faces
        row = []
        col = []
        val = []
        self.rhs = np.zeros(n)
        if variant.startswith('GMD13_'):
            gmd13_variant = variant[6:]
            stokes_system_GMD13(self.tree,val,col,row,self.rhs,self.benchmark.eta,self.benchmark.vx_boundary,self.benchmark.vy_boundary,self.benchmark.f_x,self.benchmark.f_y,self.Kbnd,self.Kcont, variant = gmd13_variant)
        else:
            raise Exception("Unknown variant %s" % variant)

        self.A = scipy.sparse.csr_matrix((val,(row,col)),shape=(n,n))

        self.system_populated = True

    def setup(self):
        """ Build auxiliary data structures.

            The tree (grid) and benchmark (problem parameters) cannot be changed after this.
            """

        # Set up tree for Stokes
        self.tree.populate_neighbors()
        [self.elements,self.faces] = self.tree.define_stokes_numbers()
        self.tree.offset_stokes_numbers(self.faces,0)
        self.corners = {}
        populate_corners(self.tree,self.corners)

        if not self.tree.check_refinement_ratios():
            raise Exception("Refinement ratios invalid!")

        self.set_up = True

    def solve(self):
        if not self.set_up:
            raise Exception("Problem must be set up")
        if self.sol:
            print("Warning: not solving again!")
            return # don't solve again
        if not self.system_populated:
            raise Exception("System must be populated")
        self.sol = scipy.sparse.linalg.spsolve(self.A,self.rhs) # Note: sol has scaled pressure
        self.p = self.sol[self.faces:] * self.Kcont
        self.v = self.sol[:self.faces]
