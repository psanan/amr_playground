# Refinement functions
# We require that these result in a maximum 2-1 ratio of refinement levels

def refine_function_uniform(node,max_level=4):
    return node.level < max_level

def refine_function_test(node,max_level=2):
    # Note: it'd be cleaner to do this with indices and levels, to avoid fp comparisons

    if node.level >= max_level:
        return False

    # Base refinement
    if node.level < max_level-2 :
        return True

    # Next level - refine "L"
    if node.level < max_level-1:
        return node.x_center <= 0.5 or node.y_center <= 0.5

    # Finest level - refine some other patches
    # assert(node.level == max_level-1)
    return (node.x_right <= 0.25 and node.y_up   <= 0.5) \
        or (node.x_right <= 0.25 and node.y_down >= 0.75) \
        or (node.x_right >= 0.75 and node.y_up   <= 0.25)

def refine_function_gmd_fig3(node,max_level=2):
    if node.level >= max_level:
        return False

    # Base refinement
    if node.level < max_level-1:
        return True

    # assert(node.level == max_level-1)
    dx_A = node.x_center - 0.3
    dy_A = node.y_center - 0.3
    inside_A = dx_A**2 + dy_A**2 < 0.25**2

    dx_B = node.x_center - 0.75
    dy_B = node.y_center - 0.6
    inside_B = dx_B**2 + dy_B**2 < 0.15**2

    return inside_A or inside_B

def refine_function_layers(node,max_level=2):
    # Note: it'd be cleaner to do this with indices and levels, to avoid fp comparisons
    if node.level >= max_level:
        return False

    # Base refinement
    if node.level < max_level-2 :
        return True

    # Middle level
    if node.level < max_level-1:
        return node.y_center <= 0.5

    # Finest level
    return node.y_center <= 0.25
