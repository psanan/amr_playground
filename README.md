# Variable-Viscosity Stokes FV/FD Narrow-Stencil Orthogonal Non-Uniform Grid Experiments

Patrick Sanan (@psanan) 2019

This is code intended to allow experimentation and analysis with non-conforming finite difference grids for Stokes problems.

First, compile a .so to evaluate the exact solution using C code.

See the Makefile, which is set up for OS X, modify if needbe, and

    make

To run a single case, make sure you have working numpy, scipy, and matplotlib, and run

    ./stokes2d.py

![stokes2d_sol](misc/stokes2d_sol.png)

To do a convergence study, run

    ./stokes2d_conv.py

![stokes2d_sol](misc/stokes2d_conv_partial.png)

One can do the above from within `ipython --pylab=auto`, e.g `%run stokes2d.py`, which gives
a MATLAB-like environment.
