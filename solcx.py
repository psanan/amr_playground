# Helpers for working with the solCx benchmark
# Includes an interface to call the exact solution (from Underworld2)
# Note that this requires you to successfully compile a shared library!

from ctypes import CDLL, c_double, c_int, POINTER
import numpy as np

class SolCx:
    """ Class to represent a reference solution.

        On initialization, sets up access to an external C library
        (This is perhaps overkill, but is at least a bit faster)
    """
    def __init__(self, sigma= 1.0, eta_A=1.0, eta_B = 1.0e6, x_c=0.5, n=1):

        self.name = 'SolCx'

        # Parameters
        self.eta_A = eta_A
        self.eta_B = eta_B
        self.x_c = x_c
        self.n = n
        self.sigma = sigma

        # Interface to C function to evaluate exact solution
        self.lib = CDLL('sols.so')
        self.evaluate_solCx = self.lib.evaluate_solCx
        # Note: these types must agree with what's in solCx.c for evaluate_solCx()
        self.evaluate_solCx.argtypes = (c_double,c_double,c_double,c_double,c_double,c_int,POINTER(c_double),POINTER(c_double),POINTER(c_double))

    def exact_vx(self, x, y):
        return self.exact_external(x,y)[0]

    def exact_vy(self, x, y):
        return self.exact_external(x,y)[1]

    def exact_p(self, x, y):
        return self.exact_external(x,y)[2]

    def eta(self, x, y):
        return self.eta_A if x <= self.x_c else self.eta_B

    def eta_min(self):
        return min(self.eta_A,self.eta_B)

    def eta_max(self):
        return max(self.eta_A,self.eta_B)

    def f_x(self, x, y):
        return 0.0

    def f_y(self, x, y):
        return -self.sigma * np.sin(self.n * np.pi * y) * np.cos(self.n * np.pi * x) # TODO ???? is this right ?????

    def vx_boundary(self, x, y):
        return 0.0

    def vy_boundary(self, x, y):
        return 0.0

    def exact_external(self, x, y):
       """ Compute the exact solution with the help of external C code (a bit faster) """
       vx = c_double(np.nan)
       vy = c_double(np.nan)
       p  = c_double(np.nan)
       self.evaluate_solCx(c_double(x),c_double(y),c_double(self.eta_A),c_double(self.eta_B),c_double(self.x_c),c_int(self.n),vx,vy,p)
       return vx.value,vy.value,p.value

    def exact_native(self, x, y):
        """ Exact solution, copied directly from Underworld2's SolCx code

        This is a bit slower than the external code (but doesn't require
        one to compile anything)
        """
        raise Exception("Not implemented")
