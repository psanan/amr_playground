# Makefile for C components (not required for Python, of course)

CC = gcc
CFLAGS = -Wall -O3 -g

LIB = sols.so
OBJ = solKz.o solCx.o

all: ${LIB}

%.o : %.c
		${CC} ${CFLAGS} -c $< -o $@

# Note that this is for OS X. On GNU/Linux you wouldn't need the -Wl,.. flag but you would need the -fPIC flag in CFLAGS
%.so : ${OBJ}
	${CC} ${CFLAGS} -shared -Wl,-install_name,$@ -o $@ $^

clean:
	rm -f ${LIB} ${OBJ}

.PHONY: all clean
