#!/usr/bin/env python
# Solve a series of Stokes problems, examining convergence to a reference solution

# IMPORTANT NOTE: these aren't true error norms! (They aren't weighted properly)

from reset_helper import reset_helper
reset_helper()

import matplotlib.pyplot as plt
import numpy as np
import scipy

from timer import Timer
from refine_funcs import *
from solkz import SolKz
from solcx import SolCx
from stokesproblem2d import StokesProblem2D

np.set_printoptions(threshold=np.inf)

# Plot setup
figsize = (10,12)
fig = None
lgd = []

case_uniform = {'name': 'Unif.', 'refine_function': lambda cell,level: refine_function_uniform(cell,level)}
case_gmd_fig3= {'name': 'Non-unif., GMD13 3a','refine_function': lambda cell,level: refine_function_gmd_fig3(cell,level+1)}

# Cases and refinement levels to use
cases      = [case_uniform,case_gmd_fig3]
ref_levels = [2,3,4,5,6,7,8]

for sol in [SolKz(),SolCx()]:
    fig = None
    for case in  cases:
        with Timer('### ' + sol.name + ', ' + case['name'] + ' ###'):

            h_maxes = []

            err_v_L1_abs = []
            err_v_L1_rel = []
            err_v_L2_abs = []
            err_v_L2_rel = []
            err_v_inf_abs = []
            err_v_inf_rel = []

            err_p_L1_abs = []
            err_p_L1_rel = []
            err_p_L2_abs = []
            err_p_L2_rel = []
            err_p_inf_abs = []
            err_p_inf_rel = []

            for ref_level in ref_levels:
                with Timer('Refinement level '+str(ref_level)):

                    pr = StokesProblem2D()
                    pr.benchmark = sol
                    pr.tree.refine(lambda cell: case['refine_function'](cell,ref_level))
                    pr.setup()
                    pr.populate_exact_solution()
                    pr.populate_stokes_system(variant = 'GMD13_A')
                    #pr.populate_stokes_system(variant = 'GMD13_E')
                    pr.solve()

                    h_maxes.append(2.0**(-pr.tree.get_min_leaf_level()))

                    err_v = pr.v_exact - pr.v
                    err_p = pr.p_exact - pr.p

                    norm_err_v_L1_abs = scipy.linalg.norm(err_v,ord=1)
                    norm_err_v_L1_rel = norm_err_v_L1_abs/scipy.linalg.norm(pr.v_exact,ord=1)
                    norm_err_v_L2_abs = scipy.linalg.norm(err_v,ord=2)
                    norm_err_v_L2_rel = norm_err_v_L2_abs/scipy.linalg.norm(pr.v_exact,ord=2)
                    norm_err_v_inf_abs = scipy.linalg.norm(err_v,ord=np.inf)
                    norm_err_v_inf_rel = norm_err_v_inf_abs/scipy.linalg.norm(pr.v_exact,ord=np.inf)

                    norm_err_p_L1_abs = scipy.linalg.norm(err_p,ord=1)
                    norm_err_p_L1_rel = norm_err_p_L1_abs/scipy.linalg.norm(pr.p_exact,ord=1)
                    norm_err_p_L2_abs = scipy.linalg.norm(err_p,ord=2)
                    norm_err_p_L2_rel = norm_err_p_L2_abs/scipy.linalg.norm(pr.p_exact,ord=2)
                    norm_err_p_inf_abs = scipy.linalg.norm(err_p,ord=np.inf)
                    norm_err_p_inf_rel = norm_err_p_inf_abs/scipy.linalg.norm(pr.p_exact,ord=np.inf)

                    err_v_L1_abs.append(norm_err_v_L1_abs)
                    err_v_L1_rel.append(norm_err_v_L1_rel)
                    err_v_L2_abs.append(norm_err_v_L2_abs)
                    err_v_L2_rel.append(norm_err_v_L2_rel)
                    err_v_inf_abs.append(norm_err_v_inf_abs)
                    err_v_inf_rel.append(norm_err_v_inf_rel)
                    err_p_L1_abs.append(norm_err_p_L1_abs)
                    err_p_L1_rel.append(norm_err_p_L1_rel)
                    err_p_L2_abs.append(norm_err_p_L2_abs)
                    err_p_L2_rel.append(norm_err_p_L2_rel)
                    err_p_inf_abs.append(norm_err_p_inf_abs)
                    err_p_inf_rel.append(norm_err_p_inf_rel)

            # Plot
            if not fig:
                fig, axes = plt.subplots(3,2,figsize=figsize)
            lgd.append(case['name'])

            axes[0,0].loglog(h_maxes,err_v_L2_rel,'x-')
            axes[0,0].set_title('Velocity, L_2 rel.')
            axes[0,0].set_ylabel('||u_h - u||_2 / ||u||_2')

            axes[0,1].loglog(h_maxes,err_p_L2_rel,'x-')
            axes[0,1].set_title('Pressure, L_2 rel.')
            axes[0,1].set_ylabel('||p_h - p||_2 / ||p||_2')

            axes[1,0].loglog(h_maxes,err_v_inf_rel,'x-')
            axes[1,0].set_title('Velocity, inf. rel.')
            axes[1,0].set_ylabel('||u_h - u||_inf / ||u||_inf')

            axes[1,1].loglog(h_maxes,err_p_inf_rel,'x-')
            axes[1,1].set_title('Pressure, inf. rel.')
            axes[1,1].set_ylabel('||p_h - p||_inf / ||p||_inf')

            axes[2,0].loglog(h_maxes,err_v_L1_rel,'x-')
            axes[2,0].set_title('Velocity, L_1 rel.')
            axes[2,0].set_xlabel('h (max)')
            axes[2,0].set_ylabel('||u_h - u||_1 / ||u||_1')

            axes[2,1].loglog(h_maxes,err_p_L1_rel,'x-')
            axes[2,1].set_title('Pressure L_1 rel.')
            axes[2,1].set_xlabel('h (max)')
            axes[2,1].set_ylabel('||p_h - p||_1 / ||p||_1')

    # Add guide lines to plot
    first_order = np.vectorize(lambda x: 2*x)(h_maxes)
    second_order = np.vectorize(lambda x: 10*x*x)(h_maxes)
    lgd.extend(['first order','second order'])
    for i in range(3):
        for j in range(2):
            axes[i,j].loglog(h_maxes,first_order,'k--')
            axes[i,j].loglog(h_maxes,second_order,'k')
            axes[i,j].legend(lgd)

    plt.suptitle(sol.name)

    # Tighten layout
    plt.tight_layout()

# Show plots
plt.show(block=False)
