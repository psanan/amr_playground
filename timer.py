import time

# https://stackoverflow.com/questions/5849800/what-is-the-python-equivalent-of-matlabs-tic-and-toc-functions
class Timer(object):
    def __init__(self, name='Stage'):
        self.name = name

    def __enter__(self):
        self.tstart = time.time()
        print('[%s]' % self.name,)

    def __exit__(self, type, value, traceback):
        print('[%s] finished in %.2f seconds' % (self.name,(time.time() - self.tstart)))
