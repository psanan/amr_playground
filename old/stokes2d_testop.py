#!/usr/bin/env python
# Test operator
# NOTE : this is an old test and may not even work any more!

import sys
import scipy
from scipy.sparse.linalg import spsolve
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from timer import Timer
from refine_funcs import *
from stokes_funcs import *
from corner_funcs import *
from quadtree import QuadTreeNode
from solkz import SolKz

# Main Parameters ##############################################################

max_level = 7

#refine_function = refine_function_uniform
#refine_function = refine_function_test
#refine_function = refine_function_layers
refine_function = refine_function_gmd_fig3

################################################################################

np.set_printoptions(threshold=np.inf)

# Plotting setup
plt.close('all')
figsize = (9,9)
plt.set_cmap('RdBu')

# Create and set up the tree
with Timer('Create tree and set up'):

    # The quad tree , as a single node
    tree = QuadTreeNode()

    # Refine the single node
    tree.refine(lambda node: refine_function(node,max_level))

    # Compute neighbors and Stokes numbering
    tree.populate_neighbors()
    [elements,faces] = tree.define_stokes_numbers()
    tree.offset_stokes_numbers(faces,0)
    print(elements,"elements and",faces,"faces")

# Draw the grid, with numbers
if True:
    if max_level < 6:
        with Timer('Plot tree'):
            fig,ax = plt.subplots(figsize=figsize)
            tree.draw(ax,draw_numbers=True)
            plt.show(block=False)

# Populate a set of corner objects for convenience
with Timer('Populate corners'):
    corners = {}
    populate_corners(tree,corners)

# Isoviscous
eta = lambda x,y: 1.0

# zero boundaries and forcing
zero = lambda x,y: 0.0

# Determine minimum grid spacing, viscosity range
with Timer("Compute Scalings"):
    eta_ref = 1.0
    h_min = 2.0**(-tree.get_max_level())
    Kbnd  = eta_ref/(h_min**2)
    Kcont = eta_ref/h_min

    print("NOTE: SETTING KBND TO ZERO")
    Kbnd = 0
    print('eta_ref=',eta_ref)
    print('h_min=',h_min)
    print('Kbnd=',Kbnd,'Kcont=',Kcont)

# Populate a system, passing some functions
# Construct three vectors, assuming we can pass to a sparse matrix constructor
with Timer('Populate system'):
    n = elements+faces
    row = []
    col = []
    val = []
    rhs = np.zeros(n)
    stokes_system(tree,val,col,row,rhs,eta,zero,zero,zero,zero,Kbnd,Kcont)

with Timer("Construct CSR from triples"):
    A = scipy.sparse.csr_matrix((val,(row,col)),shape=(n,n))
    print("A has size",n,",",A.nnz,"nonzero entries")

if max_level < 3:
    with Timer("Analyze matrix (slow)"):
        Adense = A.todense()
        print("A has rank",np.linalg.matrix_rank(Adense),"and condition number",np.linalg.cond(Adense))

## Plot nonzero structure of matrix
#with Timer('Spy'):
#    plt.figure(figsize=figsize)
#    plt.spy(A,marker='.')

# Test cases setup
case = 4
if case == 4:
    # "parallelogram problem" - gives errors which are O(1) wrt h!!
    # Generate vector to apply to
    x = np.zeros(n)
    vx_field = lambda x,y: (y**2) * ((1.0-y)**2)
    tree.populate_face_vector_x(x,vx_field)

    # Expected: zero
    expected_x = lambda x,y: 0.0 # WRONG
    expected_y = lambda x,y: 0.0
if case == 3:
    # Generate vector to apply to
    x = np.zeros(n)
    vy_field = lambda x,y: (y+1.0)**4
    tree.populate_face_vector_y(x,vy_field)

    # Expected: 2x second derivative of vy, in vy slot
    expected_x = lambda x,y: 0.0
    expected_y = lambda x,y: 2.0 * 12.0*((y+1.0)**2)
if case == 2:
    # Generate vector to apply to
    x = np.zeros(n)
    vx_field = lambda x,y: (x+1.0)**4
    tree.populate_face_vector_x(x,vx_field)

    # Expected: 2x second derivative of vx, in vx slot
    expected_x = lambda x,y: 2.0 * 12.0*((x+1.0)**2)
    expected_y = lambda x,y: 0.0

elif case == 1:
    # Generate vector to apply to
    x = np.zeros(n)
    pfield = lambda x,y: (x+1.0)**3 + (y+1.0)**3
    tree.populate_element_vector(x,pfield)

    # Expected: -Kcont times gradient
    expected_x = lambda x,y: -3.0*Kcont*((x+1.0)**2)
    expected_y = lambda x,y: -3.0*Kcont*((y+1.0)**2)

with Timer("Generate expected solution and mask"):
    y_exact = np.zeros(n)
    tree.populate_face_vector_x(y_exact,expected_x)
    tree.populate_face_vector_y(y_exact,expected_y)
    stokes_system_GMD13_mask(tree,y_exact)

# Draw test vector
if False:
    with Timer('Draw test vector'):
        fig,axes = plt.subplots(2,2,figsize=figsize)
        tree.draw_element_vector(axes[1,0],x[faces:])
        axes[1,0].set_title('x_p')
        tree.draw_face_vector_x(axes[0,0],x[:faces])
        axes[0,0].set_title('x_vx')
        tree.draw_face_vector_y(axes[0,1],x[:faces])
        axes[0,1].set_title('x_vy ')

# Plot Expected solution, at cell centers
if True:
    with Timer('Plot Expected'):
        fig,axes = plt.subplots(2,2,figsize=figsize)
        tree.draw_element_vector(axes[1,0],y_exact[faces:])
        axes[1,0].set_title('exact: p slots (masked)')
        #tree.draw_face_vector_x(axes[0,0],y_exact[:faces])
        #axes[0,0].set_title('exact: vx slots (masked)')
        #tree.draw_face_vector_y(axes[0,1],y_exact[:faces])
        #axes[0,1].set_title('exact: vy slots (masked)')
        tree.draw(axes[1,0])
        #tree.draw(axes[0,0])
        #tree.draw(axes[0,1])

# Apply operator
with Timer('Apply operator and mask'):
    y = A * x
    stokes_system_GMD13_mask(tree,x)

# error
with Timer("Compute Error"):
    err = y-y_exact

# Plot Solution
if False:
    with Timer('Plot Result'):
        fig,axes = plt.subplots(2,2,figsize=figsize)
        tree.draw_element_vector(axes[1,0],y[faces:])
        axes[1,0].set_title('y: p slots (masked)')
        #tree.draw_face_vector_x(axes[0,0],y[:faces])
        #axes[0,0].set_title('y: vx slots (masked)')
        #tree.draw_face_vector_y(axes[0,1],y[:faces])
        #axes[0,1].set_title('y: vy slots  (masked)')
        tree.draw(axes[1,0])
        #tree.draw(axes[0,1])
        #tree.draw(axes[0,0])

# Plot Error
with Timer("Plot error at cell/face centers"):
    fig,axes = plt.subplots(2,2,figsize=figsize)
    tree.draw_element_vector(axes[1,0],err[faces:])
    axes[1,0].set_title('error: p slots (masked)')
    tree.draw_face_vector_x(axes[0,0],err[:faces])
    axes[0,0].set_title('error: vx slots (masked)')
    tree.draw_face_vector_y(axes[0,1],err[:faces])
    axes[0,1].set_title('error: vy slots (masked)')
    tree.draw(axes[0,1])
    #tree.draw(axes[1,0])
    tree.draw(axes[0,0])

# Test masking
if False:
    with Timer("Masking test"):
        x = np.empty(len(x))
        x[:] = 100.0
        stokes_system_GMD13_mask(tree,x,val=-100.0)
        fig,axes = plt.subplots(2,2,figsize=figsize)
        tree.draw_element_vector(axes[1,0],x[faces:])
        tree.draw_face_vector_x(axes[0,0],x)
        tree.draw_face_vector_y(axes[0,1],x)
        tree.draw(axes[0,1])
        tree.draw(axes[1,0])
        tree.draw(axes[0,0])

# Show all plots
plt.show(block=False)

