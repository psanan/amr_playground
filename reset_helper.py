def reset_helper():
    """ Clear variables, modules, and close figures (useful if using from %run in IPython) """
    import matplotlib.pyplot as plt
    plt.close('all')
    try:
        from IPython import get_ipython
        ipython = get_ipython()
        if ipython != None:
            ipython.magic('reset -sf')
            ipython.magic("reload_ext autoreload")
            ipython.magic("autoreload 2")
    except ImportError:
        pass
