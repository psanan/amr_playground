import numpy as np

from quadtree import QuadTreeCell

# Functions for computing Stokes systems. We try to keep as many functions
# as possible agnostic to which of several choices of stencil we use.

def Edot_xx(cell):
    hx = cell.x_right - cell.x_left
    cols = [cell.dof_right,cell.dof_left]
    vals = [1.0/hx, -1.0/hx]
    return cols,vals


def Edot_yy(cell):
    hy = cell.y_up - cell.y_down
    cols = [cell.dof_up,cell.dof_down]
    vals = [1.0/hy, -1.0/hy]
    return cols,vals


def dvx_dy(corner):
    # Does not address hanging cells, directly, yet
    if corner.hanging:
        raise Exception("Not implemented - perhaps you wanted to average two other stresses")

    cellA = corner.cellA
    cellB = corner.cellB
    cellC = corner.cellC
    cellD = corner.cellD

    # Determine which levels are "fine" and "coarse"
    levels = []
    for cell in [cellA,cellB,cellC,cellD]:
        if cell:
            levels.append(cell.level)
    fine_level = max(levels)
    coarse_level = min(levels)
    if fine_level - coarse_level not in [0,1]:
        raise Exception("Error - coarse and fine levels differ by more than 1")
    if cellA:
        Acoarse = cellA.level == coarse_level
        Afine = not Acoarse
    if cellB:
        Bcoarse = cellB.level == coarse_level
        Bfine = not Bcoarse
    if cellC:
        Ccoarse = cellC.level == coarse_level
        Cfine = not Ccoarse
    if cellD:
        Dcoarse = cellD.level == coarse_level
        Dfine = not Dcoarse

    # Check that we aren't doing the corner case (not supported)
    cell_count = (cellA != None) + (cellB != None) + (cellC != None) + (cellD != None)
    if cell_count not in [2,4]:
        raise Exception("Unsupported number of cells (" + str(cell_count) +") provided")
        # Note: if desired, the case where 1 cell (in the corner) is provided is simple - just return nothing (empty arrays) if using free slip.

    # Choose which velocity dof to use and its coordinate
    # *** Taras Style ***
    # always choose the close, short edge if possible, except when we have 3 coarse cells, e.g. "J"
    # in GeryaMayDuretz2013, Fig 2. In that case, use all the long edges.
    #

    coarse_count = (cellA != None and Acoarse) + (cellB != None and Bcoarse) + (cellC != None and Ccoarse) + (cellD != None and Dcoarse)
    choose_coarse = coarse_count == 3

    # Note that for strict  comparison with some reference code from Thibault,
    # one should turn this off! (It doesnt' seem to make a huge difference..)
    #choose_coarse = False

    # A-B
    if not cellA and not cellB:
        dofAB = None
        yAB = None
    elif not cellA:
        dofAB = cellB.dof_left
        yAB = cellB.y_center
    elif not cellB:
        dofAB = cellA.dof_right
        yAB = cellA.y_center
    elif choose_coarse:
        if Afine:
            dofAB = cellB.dof_left
            yAB = cellB.y_center
        else:
            dofAB = cellA.dof_right
            yAB = cellA.y_center
    else:
        if Afine:
            dofAB = cellA.dof_right
            yAB = cellA.y_center
        else:
            dofAB = cellB.dof_left
            yAB = cellB.y_center

    # C-D
    if not cellC and not cellD:
        dofCD = None
        yCD = None
    elif not cellC:
        dofCD = cellD.dof_left
        yCD = cellD.y_center
    elif not cellD:
        dofCD = cellC.dof_right
        yCD = cellC.y_center
    elif choose_coarse:
        if Cfine:
            dofCD = cellD.dof_left
            yCD = cellD.y_center
        else:
            dofCD = cellC.dof_right
            yCD = cellC.y_center
    else:
        if Cfine:
            dofCD = cellC.dof_right
            yCD = cellC.y_center
        else:
            dofCD = cellD.dof_left
            yCD = cellD.y_center

    # Spacing and entries
    cols = []
    vals = []
    if yAB and yCD:
        hy = yAB - yCD
        cols.extend([dofAB,dofCD])
        vals.extend([1.0/hy,-1.0/hy])

    return cols,vals


def dvy_dx(corner):
    # Does not address hanging nodes (corners), directly, yet
    if corner.hanging:
        print(" WARNING !!!!! Txy on hanging corner not implemented - perhaps you wanted to average two other stresses. Returning ZERO STRESS")
        return [],[]

    cellA = corner.cellA
    cellB = corner.cellB
    cellC = corner.cellC
    cellD = corner.cellD

    # Determine which levels are "fine" and "coarse"
    levels = []
    for cell in [cellA,cellB,cellC,cellD]:
        if cell:
            levels.append(cell.level)
    fine_level = max(levels)
    coarse_level = min(levels)
    if fine_level - coarse_level not in [0,1]:
        raise Exception("Error - coarse and fine levels differ by more than 1")
    if cellA:
        Acoarse = cellA.level == coarse_level
        Afine = not Acoarse
    if cellB:
        Bcoarse = cellB.level == coarse_level
        Bfine = not Bcoarse
    if cellC:
        Ccoarse = cellC.level == coarse_level
        Cfine = not Ccoarse
    if cellD:
        Dcoarse = cellD.level == coarse_level
        Dfine = not Dcoarse

    # Check that we aren't doing the corner case (not supported)
    cell_count = (cellA != None) + (cellB != None) + (cellC != None) + (cellD != None)
    if cell_count not in [2,4]:
        raise Exception("Unsupported number of cells (" + str(cell_count) +") provided")
        # Note: if desired, the case where 1 cell (in the corner) is provided is simple - just return nothing (empty arrays) if using free slip.

    # Choose which velocity dof to use and its coordinate
    # *** Taras Style ***
    # always choose the close, short edge if possible, except when we have 3 coarse cells, e.g. "J"
    # in GeryaMayDuretz2013, Fig 2. In that case, use all the long edges.

    coarse_count = (cellA != None and Acoarse) + (cellB != None and Bcoarse) + (cellC != None and Ccoarse) + (cellD != None and Dcoarse)
    choose_coarse = coarse_count == 3

    # Note that for strict  comparison with some reference code from Thibault,
    # one should turn this off! (It doesnt' seem to make a huge difference..)
    #choose_coarse = False

    # B-D
    if not cellB and not cellD:
        dofBD = None
        xBD = None
    elif not cellB:
        dofBD = cellD.dof_up
        xBD = cellD.x_center
    elif not cellD:
        dofBD = cellB.dof_down
        xBD = cellB.x_center
    elif choose_coarse:
        if Bfine:
            dofBD = cellD.dof_up
            xBD = cellD.x_center
        else:
            dofBD = cellB.dof_down
            xBD = cellB.x_center
    else:
        if Bfine:
            dofBD = cellB.dof_down
            xBD = cellB.x_center
        else:
            dofBD = cellD.dof_up
            xBD = cellD.x_center

    # A-C
    if not cellA and not cellC:
        dofAC = None
        xAC = None
    elif not cellA:
        dofAC = cellC.dof_up
        xAC = cellC.x_center
    elif not cellC:
        dofAC = cellA.dof_down
        xAC = cellA.x_center
    elif choose_coarse:
        if Afine:
            dofAC = cellC.dof_up
            xAC = cellC.x_center
        else:
            dofAC = cellA.dof_down
            xAC = cellA.x_center
    else:
        if Afine:
            dofAC = cellA.dof_down
            xAC = cellA.x_center
        else:
            dofAC = cellC.dof_up
            xAC = cellC.x_center

    # Spacing and entries
    cols = []
    vals = []
    if xAC and xBD:
        hx = xBD - xAC
        cols.extend([dofBD,dofAC])
        vals.extend([1.0/hx, -1.0/hx])

    return cols,vals


def Edot_xy_times_two(corner):
    """ Compute slots and coefficients for 2x strain rate xy component at a corner (vertex),

    That is, this computes dv_x/dy + dv_y/dx (missing usual factor of 1/2)

    A | B        Hup             | B
    --x--       --x--      Hleft x--      etc.
    C | D       C | D            | D

    Cells can and should be None if x is on a domain boundary. In this
    case a free-slip (zero stress) condition is imposed.

    Note that involves some choices about which velocity components to use
    when the cells (elements) are not all the same size.

    """

    cellA = corner.cellA
    cellB = corner.cellB
    cellC = corner.cellC
    cellD = corner.cellD

    # Some consistency checking (assertions) - can remove
    if cellB and cellB.boundary_left and cellA:
        raise Exception("cellA should be None")
    if cellD and cellD.boundary_left and cellC:
        raise Exception("cellC should be None")
    if cellA and cellA.boundary_right and cellB:
        raise Exception("cellB should be None")
    if cellC and cellC.boundary_right and cellD:
        raise Exception("cellD should be None")
    if cellA and cellA.boundary_down and cellC:
        raise Exception("cellC should be None")
    if cellB and cellB.boundary_down and cellD:
        raise Exception("cellD should be None")
    if cellC and cellC.boundary_up and cellA:
        raise Exception("cellA should be None")
    if cellD and cellD.boundary_up and cellB:
        raise Exception("cellB should be None")

    cols = []
    vals = []

    cols_dvx_dy, vals_dvx_dy = dvx_dy(corner)
    cols.extend(cols_dvx_dy)
    vals.extend(vals_dvx_dy)

    cols_dvy_dx, vals_dvy_dx = dvy_dx(corner)
    cols.extend(cols_dvy_dx)
    vals.extend(vals_dvy_dx)

    return cols,vals


def stokes_system_GMD13(cell: QuadTreeCell, val, col, row, val_rhs, eta,
                        vx_boundary, vy_boundary, f_x, f_y, Kbnd, Kcont,
                        variant = 'A'):
    """ Build a Stokes system, as in GeryaMayDuretz2013

    This proceeds by populating row, col, and value arrays, which will
    be used to populate a sparse matrix.  Ordering must thus be consistent
    between these arrays.

    A separate array for the righthand side is also populated.

    We attempt to label things *** Gerya Style *** when they would differ
    from other implementations, but probably dont catch all such cases

    Note that we solve for a scaled pressure (see Gerya2009, problem 7.2),
    using Kcont, so that the norms of the solution components are comparable
    across fields.  Similarly, we use a scaling (Kbnd) for boundary cells and
    equality constraints.
    """

    if not cell.is_leaf():
        # Only leaf cells participate in the system
        for child in cell.children:
            stokes_system_GMD13(child,val,col,row,val_rhs,eta,vx_boundary,vy_boundary,f_x,f_y,Kbnd,Kcont, variant)
    else:

        # Check for split edges on the "big" element
        # (And recall that here we only consider 2-1 refinement ratios)
        split_left  = cell.hang_left  != None
        split_right = cell.hang_right != None
        split_up    = cell.hang_up    != None
        split_down  = cell.hang_down  != None

        # Check for being on the "small" side of an irregular edge
        small_left  = not cell.boundary_left  and not cell.neighbor_left
        small_right = not cell.boundary_right and not cell.neighbor_right
        small_up    = not cell.boundary_up    and not cell.neighbor_up
        small_down  = not cell.boundary_down  and not cell.neighbor_down

        if cell.boundary_left and cell.boundary_down:
            # Pin bottom left pressure
            row.extend([cell.dof_element])
            col.extend([cell.dof_element])
            val.extend([Kbnd])
            val_rhs[cell.dof_element] = 0.0
        else:
            # Continuity
            hx = cell.x_right - cell.x_left
            hy = cell.y_up    - cell.y_down
            row.extend([cell.dof_element]*4)
            col.extend([cell.dof_right,cell.dof_left,cell.dof_up,cell.dof_down])
            val.extend([Kcont/hx,-Kcont/hx,Kcont/hy,-Kcont/hy])
            val_rhs[cell.dof_element] = 0.0

        # Velocity Dirichlet boundaries (free slip)
        if cell.boundary_right:
            row.extend([cell.dof_right])
            col.extend([cell.dof_right])
            val.extend([Kbnd])
            val_rhs[cell.dof_right] = Kbnd * vx_boundary(cell.x_right,cell.y_center)
        if cell.boundary_left:
            row.extend([cell.dof_left])
            col.extend([cell.dof_left])
            val.extend([Kbnd])
            val_rhs[cell.dof_left] = Kbnd * vx_boundary(cell.x_left,cell.y_center)
        if cell.boundary_up:
            row.extend([cell.dof_up])
            col.extend([cell.dof_up])
            val.extend([Kbnd])
            val_rhs[cell.dof_up] = Kbnd * vy_boundary(cell.x_center,cell.y_up)
        if cell.boundary_down:
            row.extend([cell.dof_down])
            col.extend([cell.dof_down])
            val.extend([Kbnd])
            val_rhs[cell.dof_down] = Kbnd * vy_boundary(cell.x_center,cell.y_down)

        # Momentum

        # Interior x-momentum (including free-slip boundaries)
        # *** Gerya Style *** we don't compute when we're a small edge on a split face
        # Enforce on right faces only if cell is "big" on a split face
        if split_right:
            cell_upright   = cell.hang_right.cellB
            cell_downright = cell.hang_right.cellD

            # Txx terms
            hx = cell_upright.x_center - cell.x_center

            eta_upright   = eta(cell_upright.x_center,  cell_upright.y_center)
            eta_downright = eta(cell_downright.x_center,cell_downright.y_center)
            eta_here      = eta(cell.x_center,          cell.y_center)

            row.extend([cell.dof_right]*6)

            col_upright,edot_upright     = Edot_xx(cell_upright)
            col_downright,edot_downright = Edot_xx(cell_downright)
            col_here,edot_here           = Edot_xx(cell)

            col.extend(col_upright)
            col.extend(col_downright)
            col.extend(col_here)

            val.extend(       (eta_upright/hx)   * np.array(edot_upright))
            val.extend(       (eta_downright/hx) * np.array(edot_downright))
            val.extend(-2.0 * (eta_here/hx)      * np.array(edot_here))

            # Pressure gradient terms
            row.extend([cell.dof_right]*3)
            col.extend([cell_upright.dof_element,cell_downright.dof_element,cell.dof_element])
            val.extend([-0.5 * Kcont/hx, -0.5*Kcont/hx, 1.0 * Kcont/hx])

            # Txy terms
            hy = cell.y_up - cell.y_down

            # Txy terms, up
            if cell.corner_upright.hanging:
                raise Exception("assertion failure")
            else:
                eta_upright = eta(cell.x_right,cell.y_up)
                col_Txy_upright,edot_upright = Edot_xy_times_two(cell.corner_upright)
                row.extend([cell.dof_right]*len(col_Txy_upright))
                col.extend(col_Txy_upright)
                val.extend((eta_upright/hy) * np.array(edot_upright))

            # Txy terms, down
            if cell.corner_downright.hanging:
                raise Exception("assertion failure")
            else:
                eta_downright = eta(cell.x_right,cell.y_down)
                col_Txy_downright,edot_downright = Edot_xy_times_two(cell.corner_downright)
                row.extend([cell.dof_right]*len(col_Txy_downright))
                col.extend(col_Txy_downright)
                val.extend(-(eta_downright/hy) * np.array(edot_downright))

            # Forcing
            val_rhs[cell.dof_right] = f_x(cell.x_right,cell.y_center)

        # Always enforce on left faces, unless "small" on a split face
        if not cell.boundary_left and not small_left:
            if split_left:
                # *** Gerya Style *** - we interpolate the stresses on the two smaller cells
                # Txx terms
                cell_upleft   = cell.hang_left.cellA
                cell_downleft = cell.hang_left.cellC
                cell_right    = cell

                hx = cell_right.x_center - cell_upleft.x_center

                eta_upleft   = eta(cell_upleft.x_center,  cell_upleft.y_center)
                eta_downleft = eta(cell_downleft.x_center,cell_downleft.y_center)
                eta_right    = eta(cell_right.x_center,   cell_right.y_center)

                row.extend([cell.dof_left]*6)

                col_upleft,edot_upleft     = Edot_xx(cell_upleft)
                col_downleft,edot_downleft = Edot_xx(cell_downleft)
                col_right,edot_right       = Edot_xx(cell_right)

                col.extend(col_upleft)
                col.extend(col_downleft)
                col.extend(col_right)

                val.extend(      -(eta_upleft/hx)   * np.array(edot_upleft) )
                val.extend(      -(eta_downleft/hx) * np.array(edot_downleft))
                val.extend( 2.0 * (eta_right/hx)    * np.array(edot_right)  )

                # Pressure gradient terms
                row.extend([cell.dof_left]*3)
                col.extend([cell_upleft.dof_element,cell_downleft.dof_element,cell_right.dof_element])
                val.extend([0.5*Kcont/hx, 0.5*Kcont/hx, -Kcont/hx])

            else:
                # Txx terms
                hx = cell.x_center - cell.neighbor_left.x_center

                # Txx terms, right
                row.extend([cell.dof_left]*2)
                col_right,edot_right = Edot_xx(cell)
                col.extend(col_right)
                eta_rightcell = eta(cell.x_center,cell.y_center)
                val.extend(2.0 * (eta_rightcell / hx) * np.array(edot_right))

                # Txx terms, left
                row.extend([cell.dof_left]*2)
                col_left,edot_left = Edot_xx(cell.neighbor_left)
                col.extend(col_left)
                eta_leftcell = eta(cell.neighbor_left.x_center,cell.neighbor_left.y_center)
                val.extend(-2.0 * (eta_leftcell / hx) * np.array(edot_left))

                # Pressure gradient terms
                hx = cell.x_center - cell.neighbor_left.x_center
                row.extend([cell.dof_left]*2)
                col.extend([cell.dof_element,cell.neighbor_left.dof_element])
                val.extend([-Kcont/hx,Kcont/hx])

            # Txy terms
            hy =  cell.y_up - cell.y_down

            # Txy terms, up
            if cell.corner_upleft.hanging:
                # *** Gerya Style *** for Txy on a hanging corner, average adjacent stresses on split face
                big_cell = cell.corner_upleft.cellHup # The larger cell above
                eta_L = eta(big_cell.x_left,big_cell.y_down)
                eta_R = eta(big_cell.x_right,big_cell.y_down)
                col_Txy_L,edot_L = Edot_xy_times_two(big_cell.corner_downleft)
                col_Txy_R,edot_R = Edot_xy_times_two(big_cell.corner_downright)
                row.extend([cell.dof_left]*len(col_Txy_L))
                row.extend([cell.dof_left]*len(col_Txy_R))
                col.extend(col_Txy_L)
                col.extend(col_Txy_R)
                val.extend(0.5 * (eta_L/hy) * np.array(edot_L))
                val.extend(0.5 * (eta_R/hy) * np.array(edot_R))
            else:
                eta_upleft = eta(cell.x_left,cell.y_up)
                col_Txy_upleft,edot_upleft = Edot_xy_times_two(cell.corner_upleft)
                row.extend([cell.dof_left]*len(col_Txy_upleft))
                col.extend(col_Txy_upleft)
                val.extend((eta_upleft/hy) * np.array(edot_upleft))

            # Txy terms, down
            if cell.corner_downleft.hanging:
                # *** Gerya Style *** for Txy on a hanging corner, average adjacent stresses on split face
                big_cell = cell.corner_downleft.cellHdown # The larger cell below
                eta_L = eta(big_cell.x_left,big_cell.y_up)
                eta_R = eta(big_cell.x_right,big_cell.y_up)
                col_Txy_L,edot_L = Edot_xy_times_two(big_cell.corner_upleft)
                col_Txy_R,edot_R = Edot_xy_times_two(big_cell.corner_upright)
                row.extend([cell.dof_left]*len(col_Txy_L))
                row.extend([cell.dof_left]*len(col_Txy_R))
                col.extend(col_Txy_L)
                col.extend(col_Txy_R)
                val.extend(-0.5 * (eta_L/hy) * np.array(edot_L))
                val.extend(-0.5 * (eta_R/hy) * np.array(edot_R))
            else:
                eta_downleft = eta(cell.x_left,cell.y_down)
                col_Txy_downleft,edot_downleft = Edot_xy_times_two(cell.corner_downleft)
                row.extend([cell.dof_left]*len(col_Txy_downleft))
                col.extend(col_Txy_downleft)
                val.extend(-(eta_downleft/hy) * np.array(edot_downleft))

            # Forcing
            val_rhs[cell.dof_left] = f_x(cell.x_left,cell.y_center)

        # Interior y-momentum (including free-slip boundaries) on down faces
        # *** Gerya Style ***
        # We don't compute this when we're a small edge on a split face
        if split_up:
            cell_upleft  = cell.hang_up.cellA
            cell_upright = cell.hang_up.cellB
            cell_here    = cell

            hy = cell_upleft.y_center - cell_here.y_center

            eta_upleft  = eta(cell_upleft.x_center, cell_upleft.y_center)
            eta_upright = eta(cell_upright.x_center,cell_upright.y_center)
            eta_here    = eta(cell_here.x_center,   cell_here.y_center)

            row.extend([cell.dof_up]*6)

            col_upleft,  val_upleft  = Edot_yy(cell_upleft)
            col_upright, val_upright = Edot_yy(cell_upright)
            col_here,    val_here    = Edot_yy(cell_here)

            col.extend(col_upleft)
            col.extend(col_upright)
            col.extend(col_here)

            val.extend(       (eta_upleft/hy)  * np.array(val_upleft))
            val.extend(       (eta_upright/hy) * np.array(val_upright))
            val.extend(-2.0 * (eta_here/hy)    * np.array(val_here))

            row.extend([cell.dof_up]*3)
            col.extend([cell_upleft.dof_element, cell_upright.dof_element, cell_here.dof_element])
            val.extend([-0.5*Kcont/hy, -0.5*Kcont/hy, Kcont/hy])

            # Txy terms
            hx = cell.x_right - cell.x_left

            # Txy terms, right
            if cell.corner_upright.hanging:
                raise Exception("assertion failure")
            else:
                eta_upright = eta(cell.x_right, cell.y_up)
                col_Txy_upright,val_edot_upright = Edot_xy_times_two(cell.corner_upright)
                row.extend([cell.dof_up]*len(col_Txy_upright))
                col.extend(col_Txy_upright)
                val.extend((eta_upright/hx) * np.array(val_edot_upright))

            # Txy terms, left
            if cell.corner_upleft.hanging:
                raise Exception("assertion failure - thought this was impossible")
            else:
                eta_upleft = eta(cell.x_left,cell.y_up)
                col_Txy_upleft,val_edot_upleft = Edot_xy_times_two(cell.corner_upleft)
                row.extend([cell.dof_up]*len(col_Txy_upleft))
                col.extend(col_Txy_upleft)
                val.extend(-(eta_upleft/hx) * np.array(val_edot_upleft))

            # Forcing
            val_rhs[cell.dof_up] = f_y(cell.x_center,cell.y_up)

        # Enforce y momentum on down face, if not a boundary and half of a split face
        if not cell.boundary_down and not small_down:
            if split_down:
                # Tyy terms
                cell_downleft  = cell.hang_down.cellC
                cell_downright = cell.hang_down.cellD
                cell_here        = cell

                hy = cell_here.y_center - cell_downright.y_center

                eta_downleft  = eta(cell_downleft.x_center, cell_downleft.y_center)
                eta_downright = eta(cell_downright.x_center,cell_downright.y_center)
                eta_here      = eta(cell_here.x_center,     cell_here.y_center)

                row.extend([cell.dof_down]*6)

                col_downleft,  val_downleft  = Edot_yy(cell_downleft)
                col_downright, val_downright = Edot_yy(cell_downright)
                col_here,      val_here      = Edot_yy(cell_here)

                col.extend(col_downleft)
                col.extend(col_downright)
                col.extend(col_here)

                val.extend(      -(eta_downleft/hy)  * np.array(val_downleft))
                val.extend(      -(eta_downright/hy) * np.array(val_downright))
                val.extend( 2.0 * (eta_here/hy)      * np.array(val_here))

                # Pressure gradient terms
                row.extend([cell.dof_down]*3)
                col.extend([cell_here.dof_element, cell_downleft.dof_element, cell_downright.dof_element])
                val.extend([-Kcont/hy, 0.5*Kcont/hy, 0.5*Kcont/hy])

            else:
                # Tyy terms
                cell_here = cell
                cell_down = cell.neighbor_down

                hy = cell_here.y_center - cell_down.y_center

                eta_here = eta(cell_here.x_center,cell_here.y_center)
                eta_down = eta(cell_down.x_center,cell_down.y_center)

                row.extend([cell.dof_down]*4)

                col_here, val_here = Edot_yy(cell_here)
                col_down, val_down = Edot_yy(cell_down)

                col.extend(col_here)
                col.extend(col_down)

                val.extend( 2.0 * (eta_here/hy) * np.array(val_here))
                val.extend(-2.0 * (eta_down/hy) * np.array(val_down))

                # Pressure gradient terms
                # *** Gerya style *** we only have momentum equations on the "big" side of irregular edges, so not that many cases to handle (or the problems in the Batty paper about taking differences on diagonals)
                hy = cell.y_center - cell.neighbor_down.y_center
                row.extend([cell.dof_down]*2)
                col.extend([cell.dof_element, cell.neighbor_down.dof_element])
                val.extend([-Kcont/hy, Kcont/hy])

            # Txy terms
            hx = cell.x_right - cell.x_left

            # Txy terms, right
            if cell.corner_downright.hanging:
                # *** Gerya Style *** for Txy on a hanging corner, average adjacent stresses on split face
                big_cell = cell.corner_downright.cellHright # The larger cell to the right
                eta_U = eta(big_cell.x_left,big_cell.y_up)
                eta_D = eta(big_cell.x_left,big_cell.y_down)
                col_Txy_U,edot_U = Edot_xy_times_two(big_cell.corner_upleft)
                col_Txy_D,edot_D = Edot_xy_times_two(big_cell.corner_downleft)
                row.extend([cell.dof_down]*len(col_Txy_U))
                row.extend([cell.dof_down]*len(col_Txy_D))
                col.extend(col_Txy_U)
                col.extend(col_Txy_D)
                val.extend(0.5 * (eta_U/hx) * np.array(edot_U))
                val.extend(0.5 * (eta_D/hx) * np.array(edot_D))
            else:
                eta_downright = eta(cell.x_right,cell.y_down)
                col_Txy_downright,val_edot_downright = Edot_xy_times_two(cell.corner_downright)
                row.extend([cell.dof_down]*len(col_Txy_downright))
                col.extend(col_Txy_downright)
                val.extend((eta_downright/hx) * np.array(val_edot_downright))

            # Txy terms, left
            if cell.corner_downleft.hanging:
                # *** Gerya Style *** for Txy on a hanging corner, average adjacent stresses on split face
                big_cell = cell.corner_downleft.cellHleft # The larger cell to the left
                eta_U = eta(big_cell.x_right,big_cell.y_up)
                eta_D = eta(big_cell.x_right,big_cell.y_down)
                col_Txy_U,edot_U = Edot_xy_times_two(big_cell.corner_upright)
                col_Txy_D,edot_D = Edot_xy_times_two(big_cell.corner_downright)
                row.extend([cell.dof_down]*len(col_Txy_U))
                row.extend([cell.dof_down]*len(col_Txy_D))
                col.extend(col_Txy_U)
                col.extend(col_Txy_D)
                val.extend(-0.5 * (eta_U/hx) * np.array(edot_U))
                val.extend(-0.5 * (eta_D/hx) * np.array(edot_D))
            else:
                eta_downleft = eta(cell.x_left,cell.y_down)
                col_Txy_downleft,val_edot_downleft = Edot_xy_times_two(cell.corner_downleft)
                row.extend([cell.dof_down]*len(col_Txy_downleft))
                col.extend(col_Txy_downleft)
                val.extend(-(eta_downleft/hx) * np.array(val_edot_downleft))

            # Forcing
            val_rhs[cell.dof_down] = f_y(cell.x_center,cell.y_down)

        # Constraints for overlapping edges
        # Note: we don't use these, to more directly compare with GMD13
        if False:
            if split_left:
                row.extend([cell.neighbor_left.children[1].dof_right]*3)
                col.extend([cell.dof_left, cell.neighbor_left.children[1].dof_right, cell.neighbor_left.children[3].dof_right])
                val.extend([Kbnd, -0.5*Kbnd, -0.5*Kbnd])
                val_rhs[cell.dof_left] = 0.0

            if split_right:
                row.extend([cell.neighbor_right.children[0].dof_left]*3)
                col.extend([cell.dof_right, cell.neighbor_right.children[0].dof_left, cell.neighbor_right.children[2].dof_left])
                val.extend([Kbnd, -0.5*Kbnd, -0.5*Kbnd])
                val_rhs[cell.dof_right] = 0.0

            if split_down:
                row.extend([cell.neighbor_down.children[0].dof_up]*3)
                col.extend([cell.dof_down, cell.neighbor_down.children[0].dof_up, cell.neighbor_down.children[1].dof_up])
                val.extend([Kbnd, -0.5*Kbnd, -0.5*Kbnd])
                val_rhs[cell.dof_down] = 0.0

            if split_up:
                row.extend([cell.neighbor_up.children[2].dof_down]*3)
                col.extend([cell.dof_up, cell.neighbor_up.children[2].dof_down, cell.neighbor_up.children[3].dof_down])
                val.extend([Kbnd, -0.5*Kbnd, -0.5*Kbnd])
                val_rhs[cell.dof_up] = 0.0

        # *** Gerya Style ***
        # Add an additional constraints for the fine-level velocities
        # on an irregular edge.
        # Note that one could be eliminated with the simpler constraint above
        # Note that we use the same "eta hat" notation which includes
        # a factor of 2. (Note that the reference MATLAB code multiplies the constraints
        # by 0.5, relative to this code)
        #
        # Note: these could perhaps be better scaled, using a 1/h term
        if variant == 'E':
            # The simplest constraints. Set each small face equal to the big face.
            if split_left:
                cell_upleft   = cell.corner_upleft.cellC
                cell_downleft = cell.corner_downleft.cellA
                row.extend([cell_upleft.dof_right]*2)
                col.extend([cell_upleft.dof_right,cell.dof_left])
                val.extend([Kbnd,-Kbnd])
                row.extend([cell_downleft.dof_right]*2)
                col.extend([cell_downleft.dof_right,cell.dof_left])
                val.extend([Kbnd,-Kbnd])
            if split_right:
                cell_upright   = cell.corner_upright.cellD
                cell_downright = cell.corner_downright.cellB
                row.extend([cell_upright.dof_left]*2)
                col.extend([cell_upright.dof_left,cell.dof_right])
                val.extend([Kbnd,-Kbnd])
                row.extend([cell_downright.dof_left]*2)
                col.extend([cell_downright.dof_left,cell.dof_right])
                val.extend([Kbnd,-Kbnd])
            if split_down:
                cell_downright = cell.corner_downright.cellC
                cell_downleft  = cell.corner_downleft.cellD
                row.extend([cell_downleft.dof_up]*2)
                col.extend([cell_downleft.dof_up,cell.dof_down])
                val.extend([Kbnd,-Kbnd])
                row.extend([cell_downright.dof_up]*2)
                col.extend([cell_downright.dof_up,cell.dof_down])
                val.extend([Kbnd,-Kbnd])
            if split_up:
                cell_upright = cell.corner_upright.cellA
                cell_upleft  = cell.corner_upleft.cellB
                row.extend([cell_upleft.dof_down]*2)
                col.extend([cell_upleft.dof_down,cell.dof_up])
                val.extend([Kbnd,-Kbnd])
                row.extend([cell_upright.dof_down]*2)
                col.extend([cell_upright.dof_down,cell.dof_up])
                val.extend([Kbnd,-Kbnd])
        elif variant == 'A':
            if split_left:
                eta_upleft   = eta(cell.x_left,cell.y_up)
                eta_downleft = eta(cell.x_left,cell.y_down)
                eta_hat      = eta_upleft + eta_downleft # includes factor of 2

                cell_upleft   = cell.corner_upleft.cellC
                cell_downleft = cell.corner_downleft.cellA

                col_upleft,   val_upleft   = dvx_dy(cell.corner_upleft)
                col_downleft, val_downleft = dvx_dy(cell.corner_downleft)

                # first constraint
                row.extend([cell_upleft.dof_right]*2)
                col.extend([cell_upleft.dof_right, cell.dof_left])
                hy = cell_upleft.y_center - cell.y_center # " delta y/4 "
                val.extend([eta_hat/hy, -eta_hat/hy])

                row.extend([cell_upleft.dof_right]*len(col_upleft))
                col.extend(col_upleft)
                val.extend(-eta_upleft * np.array(val_upleft))

                row.extend([cell_upleft.dof_right]*len(col_downleft))
                col.extend(col_downleft)
                val.extend(-eta_downleft * np.array(val_downleft))

                # second constraint
                row.extend([cell_downleft.dof_right]*2)
                col.extend([cell.dof_left, cell_downleft.dof_right])
                hy = cell.y_center - cell_downleft.y_center # " delta y/4 "
                val.extend([eta_hat/hy, -eta_hat/hy])

                row.extend([cell_downleft.dof_right]*len(col_upleft))
                col.extend(col_upleft)
                val.extend(-eta_upleft * np.array(val_upleft))

                row.extend([cell_downleft.dof_right]*len(col_downleft))
                col.extend(col_downleft)
                val.extend(-eta_downleft * np.array(val_downleft))

            if split_right:
                eta_upright   = eta(cell.x_right,cell.y_up)
                eta_downright = eta(cell.x_right,cell.y_down)
                eta_hat       = eta_upright + eta_downright # includes factor of 2

                cell_upright   = cell.corner_upright.cellD
                cell_downright = cell.corner_downright.cellB

                col_upright,   val_upright   = dvx_dy(cell.corner_upright)
                col_downright, val_downright = dvx_dy(cell.corner_downright)

                # first constraint
                row.extend([cell_upright.dof_left]*2)
                col.extend([cell_upright.dof_left,cell.dof_right])
                hy = cell_upright.y_center - cell.y_center# " delta y / 4 "
                val.extend([eta_hat/hy, -eta_hat/hy])

                row.extend([cell_upright.dof_left]*len(col_upright))
                col.extend(col_upright)
                val.extend(-eta_upright * np.array(val_upright))

                row.extend([cell_upright.dof_left]*len(col_downright))
                col.extend(col_downright)
                val.extend(-eta_downright * np.array(val_downright))

                # second constraint
                row.extend([cell_downright.dof_left]*2)
                col.extend([cell.dof_right, cell_downright.dof_left])
                hy = cell.y_center - cell_downright.y_center # " delta y / 4 "
                val.extend([eta_hat/hy, -eta_hat/hy])

                row.extend([cell_downright.dof_left]*len(col_upright))
                col.extend(col_upright)
                val.extend(-eta_upright * np.array(val_upright))

                row.extend([cell_downright.dof_left]*len(col_downright))
                col.extend(col_downright)
                val.extend(-eta_downright * np.array(val_downright))

            if split_down:
                eta_downright = eta(cell.x_right,cell.y_down)
                eta_downleft  = eta(cell.x_left,cell.y_down)
                eta_hat       = eta_downright + eta_downleft

                cell_downright = cell.corner_downright.cellC
                cell_downleft  = cell.corner_downleft.cellD

                col_downright,val_downright = dvy_dx(cell.corner_downright)
                col_downleft, val_downleft  = dvy_dx(cell.corner_downleft)

                # first constraint
                row.extend([cell_downleft.dof_up]*2)
                col.extend([cell_downleft.dof_up,cell.dof_down])
                hx = cell_downleft.x_center - cell.x_center# "delta x / 4"
                val.extend([eta_hat/hx, -eta_hat/hx])

                row.extend([cell_downleft.dof_up]*len(col_downright))
                col.extend(col_downright)
                val.extend(-eta_downright * np.array(val_downright))

                row.extend([cell_downleft.dof_up]*len(col_downleft))
                col.extend(col_downleft)
                val.extend(-eta_downleft * np.array(val_downleft))

                # second constraint
                row.extend([cell_downright.dof_up]*2)
                col.extend([cell.dof_down, cell_downright.dof_up])
                hx = cell.x_center - cell_downright.x_center # "delta x / 4"
                val.extend([eta_hat/hx, -eta_hat/hx])

                row.extend([cell_downright.dof_up]*len(col_downright))
                col.extend(col_downright)
                val.extend(-eta_downright * np.array(val_downright))

                row.extend([cell_downright.dof_up]*len(col_downleft))
                col.extend(col_downleft)
                val.extend(-eta_downleft * np.array(val_downleft))

            if split_up:
                eta_upright = eta(cell.x_right,cell.y_up)
                eta_upleft  = eta(cell.x_left,cell.y_up)
                eta_hat     = eta_upright + eta_upleft

                cell_upright = cell.corner_upright.cellA
                cell_upleft  = cell.corner_upleft.cellB

                col_upright, val_upright = dvy_dx(cell.corner_upright)
                col_upleft,  val_upleft  = dvy_dx(cell.corner_upleft)

                # first constraint
                row.extend([cell_upleft.dof_down]*2)
                col.extend([cell_upleft.dof_down,cell.dof_up])
                hx = cell_upleft.x_center - cell.x_center# "delta x /4 "
                val.extend([eta_hat/hx, -eta_hat/hx])

                row.extend([cell_upleft.dof_down]*len(col_upright))
                col.extend(col_upright)
                val.extend(-eta_upright * np.array(val_upright))

                row.extend([cell_upleft.dof_down]*len(col_upleft))
                col.extend(col_upleft)
                val.extend(-eta_upleft * np.array(val_upleft))

                # second constraint
                row.extend([cell_upright.dof_down]*2)
                col.extend([cell.dof_up,cell_upright.dof_down])
                hx = cell.x_center - cell_upright.x_center # "delta x /4 "
                val.extend([eta_hat/hx, -eta_hat/hx])

                row.extend([cell_upright.dof_down]*len(col_upright))
                col.extend(col_upright)
                val.extend(-eta_upright * np.array(val_upright))

                row.extend([cell_upright.dof_down]*len(col_upleft))
                col.extend(col_upleft)
                val.extend(-eta_upleft * np.array(val_upleft))
        else:
            raise Exception("Unrecognized variant %s" % variant)
            

# For debugging - mask out (zero) boundary and constraint equations in the Stokes system,
# as in the function above, in a vector x
def stokes_system_GMD13_mask(cell,x,val=0.0):
    if cell.is_leaf():
        # Pin top right pressure
        if cell.boundary_right and cell.boundary_up:
            x[cell.dof_element] = val

        # Velocity Dirichlet boundaries (free slip)
        if cell.boundary_right:
            x[cell.dof_right] = val
        if cell.boundary_left:
            x[cell.dof_left] = val
        if cell.boundary_up:
            x[cell.dof_up] = val
        if cell.boundary_down:
            x[cell.dof_down] = val

        # constraints on small sides of split edges
        if cell.hang_left != None:
            x[cell.hang_left.cellA.dof_right] = val
            x[cell.hang_left.cellC.dof_right] = val
        if cell.hang_right != None:
            x[cell.hang_right.cellB.dof_left] = val
            x[cell.hang_right.cellD.dof_left] = val
        if  cell.hang_up != None:
            x[cell.hang_up.cellA.dof_down] = val
            x[cell.hang_up.cellB.dof_down] = val
        if cell.hang_down != None:
            x[cell.hang_down.cellC.dof_up] = val
            x[cell.hang_down.cellD.dof_up] = val
    else:
        for child in cell.children:
            stokes_system_GMD13_mask(child,x,val)
