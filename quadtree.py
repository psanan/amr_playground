import matplotlib.patches as patches
import matplotlib.pyplot as plt

from corner_funcs import *
from stokes_funcs import *

# Terminology:
# We use the term "corner" for the vertices in the mesh (also often called "nodes")
# We use the term "neighbor" to mean an adjacent cell (element) of the *same level*

class QuadTreeCell (object):
    """
    pos, relative to parent:

    0 | 1
    -----
    2 | 3

    """

    def __init__(self, parent=None, pos=None):
        self.parent         = parent
        self.children       = None
        self.level          = self.parent.level + 1 if self.parent else 0
        self.pos            = pos

        # Determine boundary status
        self.boundary_left  = self.level == 0 or (self.parent.boundary_left  and self.pos in [0,2])
        self.boundary_right = self.level == 0 or (self.parent.boundary_right and self.pos in [1,3])
        self.boundary_up    = self.level == 0 or (self.parent.boundary_up    and self.pos in [0,1])
        self.boundary_down  = self.level == 0 or (self.parent.boundary_down  and self.pos in [2,3])

        # Slots for references to neighboring cells (to populate later)
        self.dof_left       = None
        self.dof_right      = None
        self.dof_up         = None
        self.dof_down       = None
        self.dof_element    = None

        # Slots for references to neighboring corners (to populate later)
        self.corner_upleft     = None
        self.corner_upright    = None
        self.corner_downleft   = None
        self.corner_downright  = None
        self.hang_left  = None
        self.hang_right = None
        self.hang_up    = None
        self.hang_down  = None

        # Compute index (redundant with coordinates)
        if not self.parent:
            self.idx_x = 0
            self.idx_y = 0
        else:
            if self.pos == 0 or self.pos == 2:
                self.idx_x = 2*self.parent.idx_x
            else:
                self.idx_x = 2*self.parent.idx_x + 1
            if self.pos == 2 or self.pos == 3:
                self.idx_y = 2*self.parent.idx_y
            else:
                self.idx_y = 2*self.parent.idx_y + 1

        # Compute coordinates (for uniform grid, could be recomputed from x_idx, y_idx, and level)
        if not self.parent:
            # Root cell is the unit square
            self.x_left = 0.0; self.x_right = 1.0;
            self.y_down = 0.0; self.y_up    = 1.0;
        else:
            # Otherwise uniformly subdivide the parent
            if self.pos == 0 or self.pos == 2:
                self.x_left  = self.parent.x_left;
                self.x_right = self.parent.x_center;
            else:
                self.x_left  = self.parent.x_center;
                self.x_right = self.parent.x_right;
            if self.pos == 0 or self.pos == 1:
                self.y_up   = self.parent.y_up;
                self.y_down = self.parent.y_center;
            else:
                self.y_down = self.parent.y_down;
                self.y_up   = self.parent.y_center;
        self.x_center = (self.x_left + self.x_right) * 0.5;
        self.y_center = (self.y_up + self.y_down) * 0.5;

    def check_refinement_ratios(self):
        """ Check that if a non-boundary cell has no neighbor at the same level, its parent does

        This is intended to confirm that no edges have a refinement difference of more than
        one level

        Note that this does not currently check for 'kitty corner' 2-level changes, which
        we also don't want.
        """
        if not self.boundary_left and not self.neighbor_left and not self.parent.neighbor_left:
            return False
        if not self.boundary_right and not self.neighbor_right and not self.parent.neighbor_right:
            return False
        if not self.boundary_up and not self.neighbor_up and not self.parent.neighbor_up:
            return False
        if not self.boundary_down and not self.neighbor_down and not self.parent.neighbor_down:
            return False
        if not self.is_leaf():
            for child in self.children:
                if not child.check_refinement_ratios():
                    return False
        return True

    def level_color(self):
      level_colors = ['k','b','g','r','y','m','c']
      return level_colors[self.level % len(level_colors)]

    def draw(self, ax, draw_numbers=False):
        if self.level == 0:
            ax.set_aspect('equal')
        ax.plot(
                [self.x_left,self.x_left,self.x_right,self.x_right,self.x_left],
                [self.y_down,self.y_up,self.y_up,self.y_down,self.y_down],
                '-',color='k')
        if self.children:
            for child in self.children:
                child.draw(ax,draw_numbers)
        if draw_numbers:
            if self.dof_element != None:
                ax.text(self.x_center,self.y_center,str(self.dof_element),color=self.level_color())
            if self.dof_right != None:
                ax.text(self.x_right,self.y_center,str(self.dof_right),color=self.level_color())
            if self.dof_left != None:
                ax.text(self.x_left,self.y_center,str(self.dof_left),color=self.level_color())
            if self.dof_up != None:
                ax.text(self.x_center,self.y_up,str(self.dof_up),color=self.level_color())
            if self.dof_down != None:
                ax.text(self.x_center,self.y_down,str(self.dof_down),color=self.level_color())

    def draw_element_vector(self,ax,v_element):
        x_list = []
        y_list = []
        triangles_list = []
        self.create_element_triangulation(x_list,y_list,triangles_list)
        x = np.array(x_list)
        y = np.array(y_list)
        triangles = np.asarray(triangles_list)
        # Here, we have one entry per cell, so we can just use v_element directly
        z = np.array([val for pair in zip(v_element, v_element) for val in pair]) # double up each entry
        ax.set_aspect('equal')
        tpc = ax.tripcolor(x,y,triangles,facecolors=z)
        plt.colorbar(tpc,ax=ax)

    def create_element_triangulation(self,x_list,y_list,triangles_list):
        """ Create lists (not np.array objects) defining a triangulation of the elements """
        if self.is_leaf():
            x_list.extend([self.x_left, self.x_right, self.x_left,
                           self.x_right,self.x_right, self.x_left,
                           ])
            y_list.extend([self.y_down, self.y_down,  self.y_up,
                           self.y_down, self.y_up,    self.y_up,
                           ])
            base = 6*len(triangles_list)/2
            triangles_list.extend([[base,base+1,base+2],[base+3,base+4,base+5]])
        else:
            for child in self.children:
                child.create_element_triangulation(x_list,y_list,triangles_list)

    def draw_face_vector_x(self,ax,v_face):
        x_list = []
        y_list = []
        triangles_list = []
        z_list = []
        self.create_x_face_triangulation(v_face,x_list,y_list,triangles_list,z_list)
        x = np.array(x_list)
        y = np.array(y_list)
        triangles = np.asarray(triangles_list)
        z = np.array(z_list)
        ax.set_aspect('equal')
        tpc = ax.tripcolor(x,y,triangles,facecolors=z)
        plt.colorbar(tpc,ax=ax)

    def create_x_face_triangulation(self,v,x_list,y_list,triangles_list,z_list):
        """ Create lists (not np.array objects) defining a triangulation of half control volumes of x-velocity faces

            Note that we pass data n v and use it to populate z_list
        """
        if self.is_leaf():
            # Four triangles per element (two to decompose each of left and right half-quads)
            x_list.extend([self.x_left,   self.x_center, self.x_left,
                           self.x_center, self.x_center, self.x_left,
                           self.x_center, self.x_right,  self.x_center,
                           self.x_right,  self.x_right,  self.x_center,
                           ])
            y_list.extend([self.y_down,   self.y_down,   self.y_up,
                           self.y_down,   self.y_up,     self.y_up,
                           self.y_down,   self.y_down,   self.y_up,
                           self.y_down,   self.y_up,     self.y_up,
                           ])
            base = 12*len(triangles_list)/4
            triangles_list.extend([[base,base+1,base+2],[base+3,base+4,base+5],[base+6,base+7,base+8],[base+9,base+10,base+11]])
            z_list.extend([v[self.dof_left],v[self.dof_left],v[self.dof_right],v[self.dof_right]])
        else:
            for child in self.children:
                child.create_x_face_triangulation(v,x_list,y_list,triangles_list,z_list)

    def draw_face_vector_y(self,ax,v_face):
        x_list = []
        y_list = []
        triangles_list = []
        z_list = []
        self.create_y_face_triangulation(v_face,x_list,y_list,triangles_list,z_list)
        x = np.array(x_list)
        y = np.array(y_list)
        triangles = np.asarray(triangles_list)
        z = np.array(z_list)
        ax.set_aspect('equal')
        tpc = ax.tripcolor(x,y,triangles,facecolors=z)
        plt.colorbar(tpc,ax=ax)

    def create_y_face_triangulation(self,v,x_list,y_list,triangles_list,z_list):
        if self.is_leaf():
            # Four triangles per element (two to decompose each of up and down half-quads)
            x_list.extend([self.x_left,   self.x_right,  self.x_left,
                           self.x_right,  self.x_right,  self.x_left,
                           self.x_left,   self.x_right,  self.x_left,
                           self.x_right,  self.x_right,  self.x_left,
                           ])
            y_list.extend([self.y_down,   self.y_down,   self.y_center,
                           self.y_down,   self.y_center, self.y_center,
                           self.y_center, self.y_center, self.y_up,
                           self.y_center, self.y_up,     self.y_up,
                           ])
            base = 12*len(triangles_list)/4
            triangles_list.extend([[base,base+1,base+2],[base+3,base+4,base+5],[base+6,base+7,base+8],[base+9,base+10,base+11]])
            z_list.extend([v[self.dof_down],v[self.dof_down],v[self.dof_up],v[self.dof_up]])
        else:
            for child in self.children:
                child.create_y_face_triangulation(v,x_list,y_list,triangles_list,z_list)

    def draw_corners(self,ax):
        """ draw lines between cell centers and any corners which are defined """
        if self.level == 0:
            ax.set_aspect('equal')
        if self.is_leaf():
            denom = float(2**Corner.max_level)
            connectionstyle = 'arc3,rad=-0.25'
            if self.corner_upleft:
                x = self.corner_upleft.cidx_x/denom
                y = self.corner_upleft.cidx_y/denom
                ax.add_patch(patches.FancyArrowPatch((x,y),(self.x_center,self.y_center),connectionstyle=connectionstyle,color='m'))
            if self.corner_downleft:
                x = self.corner_downleft.cidx_x/denom
                y = self.corner_downleft.cidx_y/denom
                ax.add_patch(patches.FancyArrowPatch((x,y),(self.x_center,self.y_center),connectionstyle=connectionstyle,color='g'))
            if self.corner_upright:
                x = self.corner_upright.cidx_x/denom
                y = self.corner_upright.cidx_y/denom
                ax.add_patch(patches.FancyArrowPatch((x,y),(self.x_center,self.y_center),connectionstyle=connectionstyle,color='b'))
            if self.corner_downright:
                x = self.corner_downright.cidx_x/denom
                y = self.corner_downright.cidx_y/denom
                ax.add_patch(patches.FancyArrowPatch((x,y),(self.x_center,self.y_center),connectionstyle=connectionstyle,color='r'))
            if self.hang_right:
                x = self.hang_right.cidx_x/denom
                y = self.hang_right.cidx_y/denom
                ax.add_patch(patches.FancyArrowPatch((x,y),(self.x_center,self.y_center),connectionstyle=connectionstyle,color='y'))
            if self.hang_left:
                x = self.hang_left.cidx_x/denom
                y = self.hang_left.cidx_y/denom
                ax.add_patch(patches.FancyArrowPatch((x,y),(self.x_center,self.y_center),connectionstyle=connectionstyle,color='c'))
            if self.hang_down:
                x = self.hang_down.cidx_x/denom
                y = self.hang_down.cidx_y/denom
                ax.add_patch(patches.FancyArrowPatch((x,y),(self.x_center,self.y_center),connectionstyle=connectionstyle,color='grey'))
            if self.hang_up:
                x = self.hang_up.cidx_x/denom
                y = self.hang_up.cidx_y/denom
                ax.add_patch(patches.FancyArrowPatch((x,y),(self.x_center,self.y_center),connectionstyle=connectionstyle,color='orange'))
        else:
            for child in self.children:
                child.draw_corners(ax)

    def populate_neighbors(self):
        self.neighbor_right = self.compute_neighbor_right()
        self.neighbor_left  = self.compute_neighbor_left()
        self.neighbor_up    = self.compute_neighbor_up()
        self.neighbor_down  = self.compute_neighbor_down()
        if self.children:
            for child in self.children:
                child.populate_neighbors()

    def compute_neighbor_right(self):
        """ Return the right cousin or sibling, if it exists.

        That is, the cell of the same level, to the right
        of this cell
        """
        if self.level == 0:
            return None
        if self.pos == 0:
            return self.parent.children[1]
        elif self.pos == 2:
            return self.parent.children[3]
        else:
            parent_right = self.parent.compute_neighbor_right() # this should actually already be stored
            if parent_right and parent_right.children:
                if self.pos == 1:
                    return parent_right.children[0]
                else: # self.pos == 3
                    return parent_right.children[2]
            else:
                return None

    def compute_neighbor_left(self):
        """ Return the left cousin or sibling, if it exists.
        """
        if self.level == 0:
            return None
        if self.pos == 1:
            return self.parent.children[0]
        elif self.pos == 3:
            return self.parent.children[2]
        else:
            parent_left = self.parent.compute_neighbor_left() # this should actually already be stored
            if parent_left and parent_left.children:
                if self.pos == 0:
                    return parent_left.children[1]
                else: # self.pos == 2
                    return parent_left.children[3]
            else:
                return None

    def compute_neighbor_down(self):
        if self.level == 0:
            return None
        if self.pos == 0:
            return self.parent.children[2]
        elif self.pos == 1:
            return self.parent.children[3]
        else:
            parent_down = self.parent.compute_neighbor_down() # this should actually already be stored
            if parent_down and parent_down.children:
                if self.pos == 2:
                    return parent_down.children[0]
                else: # self.pos == 3
                    return parent_down.children[1]
            else:
                return None

    def compute_neighbor_up(self):
        if self.level == 0:
            return None
        if self.pos == 2:
            return self.parent.children[0]
        elif self.pos == 3:
            return self.parent.children[1]
        else:
            parent_up = self.parent.compute_neighbor_up() # this should actually already be stored
            if parent_up and parent_up.children:
                if self.pos == 0:
                    return parent_up.children[2]
                else: # self.pos == 1
                    return parent_up.children[3]
            else:
                return None

    def is_leaf(self):
        return not self.children

    def refine(self,refine_function):
        """ Refine as a function of this cell's data (e.g. level and coordinates) """
        if refine_function(self):
            self.children = [None]*4
            for pos in range(4):
                self.children[pos] = QuadTreeCell(self,pos)
                self.children[pos].refine(refine_function)

    def refine_uniform(self,n_refine=1):
        """ Perform a DFS, refining all leaves n_refine times """
        if n_refine < 1:
            return
        if self.is_leaf():
            self.children = [None]*4
            for pos in range(4):
                self.children[pos] = QuadTreeCell(self,pos)
            n_refine_new = n_refine-1
        else:
            n_refine_new = n_refine
        for child in self.children:
            child.refine_uniform(n_refine=n_refine_new)

    def define_stokes_numbers(self,_element_count=0,_face_count=0):
        """ Populate indices for where in a global system the element and face dof for this cell live.

        We use a naive algorithm. If a cell is a leaf, we number. Otherwise, we
        number its children (depth-first search).

        Element numbering is simple. For face numbering, we check to see if a
        neighboring cell of the same refinement has already defined the number.
        If it has, we use that number, and if not, assign a new one here.

        Note: we always number all velocities, thus introducing
        redundant values on split edges. These may not actually
        be needed to be included in a real implementation, but
        we keep the around as they are used (notationally, at least)
        in some of the algorithms we'd like to consider.

        """
        element_count = _element_count
        face_count = _face_count

        if self.children:
            for child in self.children:
                [element_count,face_count] = child.define_stokes_numbers(element_count,face_count)
        else:
            self.dof_element = element_count
            element_count = element_count + 1

            if self.neighbor_left and self.neighbor_left.dof_right:
                self.dof_left = self.neighbor_left.dof_right
            else:
                self.dof_left = face_count
                face_count = face_count + 1

            if self.neighbor_right and self.neighbor_right.dof_left:
                self.dof_right = self.eighbor_right.dof_left
            else:
                self.dof_right = face_count
                face_count = face_count + 1

            if self.neighbor_down and self.neighbor_down.dof_up:
                self.dof_down = self.neighbor_down.dof_up
            else:
                self.dof_down = face_count
                face_count = face_count + 1

            if self.neighbor_up and self.neighbor_up.dof_down:
                self.dof_up = self.neighbor_up.dof_down
            else:
                self.dof_up = face_count
                face_count = face_count + 1

        return [element_count,face_count]

    def offset_stokes_numbers(self,offset_element,offset_faces):
        if self.dof_element != None:
            self.dof_element = self.dof_element + offset_element
        if self.dof_left != None:
            self.dof_left = self.dof_left + offset_faces
        if self.dof_right != None:
            self.dof_right = self.dof_right + offset_faces
        if self.dof_up != None:
            self.dof_up = self.dof_up + offset_faces
        if self.dof_down != None:
            self.dof_down = self.dof_down + offset_faces
        if not self.is_leaf():
            for child in self.children:
                child.offset_stokes_numbers(offset_element,offset_faces)

    def populate_element_vector(self,v_element,func,offset=0):
        if self.is_leaf():
            v_element[self.dof_element + offset] = func(self.x_center,self.y_center)
        else:
            for child in self.children:
                child.populate_element_vector(v_element,func,offset)

    def populate_face_vector_x(self,v,func,offset=0):
        if self.is_leaf():
            v[self.dof_left + offset]  = func(self.x_left,self.y_center)
            v[self.dof_right + offset] = func(self.x_right,self.y_center)
        else:
            for child in self.children:
                child.populate_face_vector_x(v,func,offset)

    def populate_face_vector_y(self,v,func,offset=0):
        if self.is_leaf():
            v[self.dof_down + offset] = func(self.x_center,self.y_down)
            v[self.dof_up + offset]   = func(self.x_center,self.y_up)
        else:
            for child in self.children:
                child.populate_face_vector_y(v,func,offset)

    def get_min_leaf_level(self):
        if self.is_leaf():
            return self.level
        else:
            min_level = None
            for child in self.children:
                min_level_child = child.get_min_leaf_level()
                min_level = min(min_level,min_level_child) if min_level else min_level_child
            return min_level

    def get_max_level(self):
        if self.is_leaf():
            max_level = self.level
        else:
            max_level = None
            for child in self.children:
                max_level_child = child.get_max_level()
                max_level =  max(max_level,max_level_child) if max_level else max_level_child
        return max_level

    def get_upper_right_cell(self):
        return self if self.is_leaf() else self.children[1].get_upper_right_cell()
